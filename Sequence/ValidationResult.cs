﻿namespace Sequence
{
    public class ValidationResult
    {
        public enum EResult 
        {
            Success,
            WrongInput,
            Mismatch ,
            DuplicateInput,
            MinimumIsNotProvided
        }

        public ValidationResult(EResult result)
        {          
            Reason = result;
        }

        public ValidationResult(EResult result, int expected, int actual)
        {
            Reason = result;
            ActualCount = actual;
            ExpectedCount = expected;
        }

        public ValidationResult(EResult result, object details)
        {
            Reason = result;
            Details = details;
        }

        public object Details { get; set; }
        public int ExpectedCount { get; set; }
        public int ActualCount { get; set; }
        public EResult Reason { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;

namespace Sequence
{
    public class SequenceValidator
    {
        private readonly int _minimumSequenceNumber;

        public SequenceValidator(int minimumSequenceNumber)
        {
            _minimumSequenceNumber = minimumSequenceNumber;
        }

        public ValidationResult Validate(string sequenceLine)
        {
            if (string.IsNullOrEmpty(sequenceLine))
                return new ValidationResult(ValidationResult.EResult.WrongInput);;

            var entities = sequenceLine.Split(new[]{' '},StringSplitOptions.RemoveEmptyEntries);

            int maxNumber = 0;

            var packets = new Dictionary<int, bool>();
            foreach (var entity in entities)
            {
                int entityNumber = 0;
                if (!int.TryParse(entity, out entityNumber))
                {
                    return new ValidationResult(ValidationResult.EResult.WrongInput);
                }

                if (packets.ContainsKey(entityNumber))
                {
                    return new ValidationResult(ValidationResult.EResult.DuplicateInput, entityNumber);
                }

                packets[entityNumber] = true;
                if (entityNumber > maxNumber)
                    maxNumber = entityNumber;
            }

            if (packets.Count != maxNumber)
            {
                return new ValidationResult(ValidationResult.EResult.Mismatch, maxNumber, packets.Count);
            }


            if (!packets.ContainsKey(_minimumSequenceNumber))
            {
                return new ValidationResult(ValidationResult.EResult.MinimumIsNotProvided, _minimumSequenceNumber);
            }

            return new ValidationResult(ValidationResult.EResult.Success, packets.Count);

        }
    }
}
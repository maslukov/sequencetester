﻿using System;
using System.Collections.Generic;

namespace Sequence
{

    class Program
    {
        static void Main(string[] args)
        {        
            int cases;
            if (ReadCaseCount(out cases))
            {
                Console.WriteLine(FormatFailure("WRONG INPUT EXPECTED NUMBER OF TEST CASES"));
                return;
            }

            var inputLines = ReadInputCases(cases);

            ValidateCases(inputLines);

            // Attention to reader:it will be impossible to submit test case where number of cases is greater than provided as first input

            Console.ReadKey();
        }

        private static List<string> ReadInputCases(int lines)
        {
            var inputLines = new List<string>();
            for (int i = 0; i < lines; i++)
            {
                inputLines.Add(Console.ReadLine());
            }
            return inputLines;
        }

        private static void ValidateCases(List<string> inputLines)
        {
            var validator = new SequenceValidator(1);
            for (int i = 0; i < inputLines.Count; i++)
            {
                var caseLine = Console.ReadLine();

                var formattedOutput = RunTestCase(validator, caseLine, i);

                Console.WriteLine(formattedOutput);
            }
        }

        private static string RunTestCase(SequenceValidator validator, string caseLine, int i)
        {
            ValidationResult result = validator.Validate(caseLine);
            return  FormatResult(result, ConvertToLineNumber(i));

        }

        private static int ConvertToLineNumber(int i)
        {
            return i + 2;
        }

        private static string FormatResult(ValidationResult result, int lineCount)
        {
            switch (result.Reason)
            {
                case ValidationResult.EResult.Success:
                    return FormatSuccess(string.Format("RECEIVED: {0}",result.Details));
                    break;
                case ValidationResult.EResult.WrongInput:
                    return FormatFailure(string.Format("WRONG INPUT (LINE {0})",lineCount));
                    break;
               case ValidationResult.EResult.Mismatch:
                    return FormatFailure(string.Format("RECEIVED: {0}, EXPECTED: {1}",result.ActualCount, result.ExpectedCount));
                    break;
                case ValidationResult.EResult.MinimumIsNotProvided:
                    return FormatFailure(string.Format("MINIMUM IS NOT FOUND"));
                    break;
               case ValidationResult.EResult.DuplicateInput:
                    return FormatFailure(string.Format("DUPLICATE ITEM: {0}", result.Details));
                    break;
                default:
                    return FormatFailure("UNKNOWN ERROR");
            }
        }

        private static string FormatSuccess(string details)
        {
            return string.Format("SUCCESS => {0}", details);
        }

        private static string FormatFailure(string details)
        {
            return string.Format("FAILURE => {0}", details);
        }

        private static bool ReadCaseCount(out int lines)
        {
            var lineCountInput = Console.ReadLine();

            return !int.TryParse(lineCountInput, out lines);
        }

      
    }
}
